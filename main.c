/* ========================================
 *   PONG
 *   500831945
 *   Author Diego Brandjes
 *   IT101A 07-01-2021
 * ========================================
*/

#include "project.h"

// Maakt soort van functie aan waarbij x een variabele wordt.
#define PIN(x) Pin_##x##_Write(1);\
        CyDelay(175);\
        Pin_##x##_Write(0);\
        CyDelay(175);        

int position = 0;
int pressA  = 0;
int pressB  = 0;
int livesA  = 5;
int livesB  = 5;
int scoreA  = 0;
int scoreB  = 0;
int loop    = 1;

// Omlaag bewegende led functie, geeft ook plek door.
static void moveD(){  
       
        PIN(10)
        position = 10;
        PIN(9)       
        position = 9;
        PIN(8)       
        position = 8;
        PIN(7)       
        position = 7;
        PIN(6)       
        position = 6;
        PIN(5)       
        position = 5;
        PIN(4)       
        position = 4;
        PIN(3)        
        position = 3;
        PIN(2)     
        position = 2;
        PIN(1)
        position = 1;  
    }

// Omhoog bewegende led functie, geeft ook plek door.
static void moveU(){  
       
        PIN(1)
        position = 1;
        PIN(2) 
        position = 2;
        PIN(3)        
        position = 3;
        PIN(4)        
        position = 4;
        PIN(5)        
        position = 5;
        PIN(6)        
        position = 6;
        PIN(7)        
        position = 7;
        PIN(8)        
        position = 8;
        PIN(9)        
        position = 9;
        PIN(10)
        position = 10;  
    }

// Einde van de game laat score zien op ledboard.
static void GameOver(){
    
    switch(scoreA){
        
        case 1:
            Pin_1_Write(1);
            break;
        case 2:
            Pin_1_Write(1);
            Pin_2_Write(1);
            break;
        case 3:
            Pin_1_Write(1);
            Pin_2_Write(1);
            Pin_3_Write(1);
            break;
        case 4:
            Pin_1_Write(1);
            Pin_2_Write(1);
            Pin_3_Write(1);
            Pin_4_Write(1);
            break;
        case 5:
            Pin_1_Write(1);
            Pin_2_Write(1);
            Pin_3_Write(1);
            Pin_4_Write(1);
            Pin_5_Write(1);            
            break;    
    }
    
    switch(scoreB){
        
        case 1:
            Pin_10_Write(1);
            break;
        case 2:
            Pin_10_Write(1);
            Pin_9_Write(1);
            break;
        case 3:
            Pin_10_Write(1);
            Pin_9_Write(1);
            Pin_8_Write(1);
            break;
        case 4:
            Pin_10_Write(1);
            Pin_9_Write(1);
            Pin_8_Write(1);
            Pin_7_Write(1);
            break;
        case 5:
            Pin_10_Write(1);
            Pin_9_Write(1);
            Pin_8_Write(1);
            Pin_7_Write(1);
            Pin_6_Write(1);            
            break;   
    } 
    
        CyDelay(5000);
        Pin_1_Write(0);
        Pin_2_Write(0);
        Pin_3_Write(0);
        Pin_4_Write(0);
        Pin_5_Write(0);
        Pin_6_Write(0);
        Pin_7_Write(0);
        Pin_8_Write(0);
        Pin_9_Write(0);
        Pin_10_Write(0);        
}

// Interrupt knoppen aanmaken, haalt leven eraf wanneer je te vroeg of te laat klikt.
CY_ISR(knopA){
if(position != 10){
    livesA = livesA - 1;
    }
}
CY_ISR(knopB){
if(position != 10){
    livesB = livesB - 1;
    }
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */ 
    UART_1_Start();
    isr_1_StartEx(knopA);
    isr_2_StartEx(knopB);
    
    // De main loop
    while(loop == 1)
    {   
        // if statement, kijken of "balletje" scoort.
        if (position == 10 && pressA != SpelerA_Read()){            
            livesB = livesB - 1;
            scoreA = scoreA + 1;
            CyDelay(1000);
                        
            // Stopt de game wanneer levens te laag zijn.
            if(livesA <= 0){
                break;
            }
            moveD();
        }        
        else if(1){
            moveD();
        }
        
        // if statement, kijken of "balletje" scoort.
        if (position == 1 && pressB != SpelerB_Read()){            
            livesA = livesA - 1;
            scoreB = scoreB + 1;
            CyDelay(1000); 
            
            // Stopt de game wanneer levens te laag zijn.
            if(livesB <= 0){
                break;
            }
            moveU();
        }             
        else if(1){
            moveU();                
        }       
    }
    
    // Roept einde van de game aan.
    GameOver();
    loop = 0;
    
    // Geeft waarden door aan UART en vervolgens naar Putty.
    UART_1_PutString("PlayerA ");
    UART_1_PutChar(scoreA + '0');
    UART_1_PutString("\n\rPlayerB ");
    UART_1_PutChar(scoreB + '0');
    UART_1_PutString("\n\rPress reset to start again.\n\r");
}

/* [] END OF FILE */
